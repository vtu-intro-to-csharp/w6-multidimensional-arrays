﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ProductOfPositiveWithEvenIndicies
{
    public class Program
    {
        public static void Main(string[] args)
        {
            int[,] matrix = InputSquareMatrix();

            int product = 1;
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    if (matrix[i, j] > 0 && (i + j) % 2 == 0)
                    {
                        product *= matrix[i, j];
                    }
                }
            }

            Console.WriteLine($"The product of postive elements with even sum of indices: {product}");
        }

        static int[,] InputSquareMatrix()
        {

            Console.WriteLine("Enter the dimensions of a matrix.");
            Console.WriteLine("Enter rows and columns number: ");
            int length = int.Parse(Console.ReadLine());

            int[,] matrix = new int[length, length];

            for (int i = 0; i < length; i++)
            {
                for (int j = 0; j < length; j++)
                {
                    Console.Write($"matrix[{i}, {j}] = ");
                    matrix[i, j] = int.Parse(Console.ReadLine());
                }
            }

            return matrix;
        }
    }
}