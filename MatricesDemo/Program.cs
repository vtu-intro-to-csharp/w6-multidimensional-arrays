﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MatricesDemo
{
    public class Program
    {
        public static void Main(string[] args)
        {
            int[,] matrix = InputMatrix();
            PrintMatrix(matrix);
        }

        static int[,] InputMatrix()
        {

            Console.WriteLine("Enter the dimensions of a matrix.");
            Console.WriteLine("Enter rows number: ");
            int rows = int.Parse(Console.ReadLine());

            Console.WriteLine("Enter columns number: ");
            int columns = int.Parse(Console.ReadLine());

            int[,] matrix = new int[rows, columns];

            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < columns; j++)
                {
                    Console.Write($"matrix[{i}, {j}] = ");
                    matrix[i, j] = int.Parse(Console.ReadLine());
                }
            }

            return matrix;
        }

        static void PrintMatrix(int[,] matrix)
        {
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    Console.Write($"{matrix[i, j]} ");
                }
                Console.WriteLine();
            }
        }
    }
}