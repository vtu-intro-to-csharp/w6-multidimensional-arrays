﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MatrixSumUnderMainDiagonal
{
    public class Program
    {
        public static void Main(string[] args)
        {
            double[,] matrix = InputSquareMatrix();

            double sum = 0;
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < i; j++)
                {
                    sum += matrix[i, j];
                }
            }

            Console.WriteLine($"Sum of elements under the main diagonal: {sum}");
        }

        static double[,] InputSquareMatrix()
        {

            Console.WriteLine("Enter the dimensions of a matrix.");
            Console.WriteLine("Enter rows and columns number: ");
            int length = int.Parse(Console.ReadLine());

            double[,] matrix = new double[length, length];

            for (int i = 0; i < length; i++)
            {
                for (int j = 0; j < length; j++)
                {
                    Console.Write($"matrix[{i}, {j}] = ");
                    matrix[i, j] = double.Parse(Console.ReadLine());
                }
            }

            return matrix;
        }
    }
}